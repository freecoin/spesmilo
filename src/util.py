import os
import os.path
from PySide.QtGui import *

def get_icon_path(icon_name):
    try:
        prefix = os.environ['SPESMILO_SHARE_PATH']
    except KeyError:
        prefix = '.'
    prefix = os.path.join(prefix, 'icons')
    return os.path.join(prefix, icon_name)

def load_icon(icon_name):
    return QIcon(get_icon_path(icon_name))

