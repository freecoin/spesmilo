import jsonrpc
from jsonrpc.proxy import JSONRPCException
import decimal

class CoreInterface:
    precision = 10**8

    def __init__(self):
        self.access = jsonrpc.ServiceProxy('http://user:password@127.0.0.1:8332')

    def transactions(self):
        fix_tran = lambda t: \
            {k: decimal.Decimal(v) / self.precision if k == 'amount' else v
             for k, v in t.iteritems()}
        # Change each instance of 'amount' to Python decimal object
        trans = self.access.listtransactions()
        return [fix_tran(t) for t in trans]

    def number_blocks(self):
        return self.access.getinfo()['blocks']

    def balance(self):
        return decimal.Decimal(self.access.getbalance()) / self.precision

    def stop(self):
        return self.access.stop()

    def validate_address(self, address):
        return self.access.validateaddress(address)['isvalid']

    def send(self, address, amount):
        return self.access.sendtoaddress(address, amount)

    def default_address(self):
        return self.access.getaccountaddress('')

    def new_address(self):
        return self.access.getnewaddress('')
    
    def is_initialised(self):
        return self.access.getinfo()['isinitialized']

