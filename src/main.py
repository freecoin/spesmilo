from PySide.QtCore import *
from PySide.QtGui import *
from PySide.QtWebKit import *
import core_interface
import cashier
import send
import util

class StartupDialog(QDialog):
    def __init__(self, parent):
        super(StartupDialog, self).__init__(parent)
        main_layout = QVBoxLayout(self)
        main_layout.addWidget(QLabel(self.title_text))
        self.progress_bar = QProgressBar()
        self.progress_bar.setMinimum(0)
        self.progress_bar.setMaximum(0)
        main_layout.addWidget(self.progress_bar)
        
        button_layout = QHBoxLayout()
        button_layout.addStretch()
        abortbtn = QPushButton('&Abort')
        abortbtn.clicked.connect(self.stop)
        button_layout.addWidget(abortbtn)
        main_layout.addLayout(button_layout)

        if parent is not None:
            self.setWindowIcon(parent.bitcoin_icon)
        self.setWindowTitle(self.title_text)

    def stop(self):
        self.hide()
        self.parent().stop()

    def closeEvent(self, event):
        self.stop()

class ConnectingDialog(StartupDialog):
    def __init__(self, parent):
        super(ConnectingDialog, self).__init__(parent)
        self.show()

    @property
    def title_text(self):
        return self.tr("Connecting...")

class DownloadingDialog(StartupDialog):
    def __init__(self, parent, checkpoint):
        super(DownloadingDialog, self).__init__(parent)
        self.progress_bar.setMaximum(checkpoint)
        self.show()

    @property
    def title_text(self):
        return self.tr("First time initialisation...")

    def set_value(self, value):
        self.progress_bar.setValue(value)

class RootWindow(QMainWindow):
    CLIENT_NONE = 0
    CLIENT_CONNECTING = 1
    CLIENT_DOWNLOADING = 2
    CLIENT_RUNNING = 3

    def __init__(self):
        super(RootWindow, self).__init__()
        self.core = core_interface.CoreInterface()
        self.bitcoin_icon = util.load_icon('bitcoin_euro.png')
        self.current_window = None

        self.state = self.CLIENT_NONE
        refresh_state_timer = QTimer(self)
        refresh_state_timer.timeout.connect(self.refresh_state)
        refresh_state_timer.start(1000)
        self.refresh_state()

    def delete_window(self):
        if self.current_window is not None:
            self.current_window.deleteLater()

    def create_connecting(self):
        self.delete_window()
        self.current_window = ConnectingDialog(self)

    def create_blockchain_progress(self, checkpoint):
        self.delete_window()
        self.current_window = DownloadingDialog(self, checkpoint)

    def create_cashier(self):
        self.delete_window()
        self.current_window = cashier.Cashier(self.core, qApp.clipboard(),
                                              self)

    def refresh_state(self):
        # number of blocks as of 13th May 2011
        checkpoint = 127357

        if self.state == self.CLIENT_NONE:
            self.state = self.CLIENT_CONNECTING
            # show initialising dialog
            self.create_connecting()
        elif self.state == self.CLIENT_CONNECTING:
            try:
                is_init = self.core.is_initialised()
            except IOError:
                error = QErrorMessage()
                error.showMessage('Internal error: failed to find freecoin daemon')
                error.exec_()
                qApp.quit()
                raise
            if is_init:
                # some voodoo here checking whether we have blocks
                if self.core.number_blocks() < checkpoint:
                    self.create_blockchain_progress(checkpoint)
                    self.state = self.CLIENT_DOWNLOADING
                else:
                    self.create_cashier()
                    self.state = self.CLIENT_RUNNING
        elif self.state == self.CLIENT_DOWNLOADING:
            number_blocks = self.core.number_blocks()
            if number_blocks < checkpoint:
                # Update progress bar
                self.current_window.set_value(number_blocks)
            else:
                self.create_cashier()
                self.state = self.CLIENT_RUNNING
        elif self.state == self.CLIENT_RUNNING:
            # Unhook timer. Finished here.
            pass

    def closeEvent(self, event):
        super(RootWindow, self).closeEvent(event)
        self.stop()

    def stop(self):
        try:
            # Keep looping until connected so we can issue the stop command
            while True:
                try:
                    self.core.stop()
                except core_interface.JSONRPCException:
                    pass
                except:
                    raise
                else:
                    break
        # Re-proprogate exception & trigger app exit so we can break out
        except:
            raise
        finally:
            qApp.quit()

if __name__ == '__main__':
    import os
    import sys

    os.system('freecoind -rpcuser=user -rpcpassword=password')
    app = QApplication(sys.argv)
    translator = QTranslator()
    #translator.load('il8n/eo_EO')
    app.installTranslator(translator)
    app.setQuitOnLastWindowClosed(False)
    rootwindow = RootWindow()
    sys.exit(app.exec_())

